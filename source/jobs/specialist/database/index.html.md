---
layout: job_page
title: "Database Specialist"
---

## Responsibilities

* Manage day-to-day maintenance of GitLab's databases
* Resolve issues with database performance and query load
* Implement tools to monitor performance and load
* Set configuration parameters to improve overall user experience (e.g. reduce query times)
* Identify scalability issues with current schema and application usage patterns
* Manage regular backups and disaster recovery
* Install and test new versions of the database
* Document answers and improves existing documentation
* Write blog posts relevant to the community

In addition, see the [Production Engineer job description](/jobs/production-engineer]).
